A simple systemd timer to run borg backups (https://borgbackup.readthedocs.io/en/stable/)

The service

- runs a backup with an auto-generated name
- prune existing backups according to some rules

Both files should be placed in `/etc/systemd/system/` and then the timer is enabled via

    systemctl enable borg.timer
    
    